package
{
	import flash.display.Sprite;
	
	import starling.core.Starling;
	
	[SWF(frameRate = "60", width="640", height = "960")]
	public class Tetris extends Sprite
	{
		private var _root:Starling;
		
		public function Tetris()
		{
			_root = new Starling(Game, stage);
			_root.antiAliasing = 1;
			_root.start();
		}
	}
}