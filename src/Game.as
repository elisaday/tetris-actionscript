package
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import ui.GameScene;
	import ui.MainMenu;
	
	public class Game extends Sprite
	{
		private var _uiMainMenu:MainMenu;
		private var _uiGameScene:GameScene;
		
		private static const ROT_X:Array = [
			[   [1,0,-1,-1],   [1,0,-1,1],      [-1,0,1,1],     [-1,0,1,-1]  ],
			[   [2,1,0,-1],    [0,1,2,1],       [-2,-1,0,1],    [0,-1,-2,-1] ],
			[   [1,0,-1,0],    [-1,0,1,2],      [-1,0,1,0],     [1,0,-1,-2]  ],
			[   [1,0,-1,-2],   [-1,0,1,2],      [1,0,-1,-2],    [-1,0,1,2]   ],
			[   [1,0,1,0],     [-1,0,-1,0],     [1,0,1,0],      [-1,0,-1,0]  ],
			[   [0,0,0,0],     [0,0,0,0],       [0,0,0,0],      [0,0,0,0]    ],
			[   [2,1,0,-1],    [-2,-1,0,1],     [2,1,0,-1],     [-2,-1,0,1]  ]];
		
		private static const ROT_Y:Array = [ 
			[   [-1,0,1,-1],    [1,0,-1,-1],    [1,0,-1,1],     [-1,0,1,1]  ],
			[   [1,0,-1,0],     [1,0,-1,-2],    [0,1,2,1],      [-2,-1,0,1] ],
			[   [2,1,0,-1],     [0,-1,-2,-1],   [-1,0,1,2],     [-1,0,1,0]  ],
			[   [1,0,1,0],      [-1,0,-1,0],    [1,0,1,0],      [-1,0,-1,0] ],
			[   [1,0,-1,-2],    [-1,0,1,2],     [1,0,-1,-2],    [-1,0,1,2]  ],
			[   [0,0,0,0],      [0,0,0,0],      [0,0,0,0],      [0,0,0,0]   ],
			[   [2,1,0,-1],     [-2,-1,0,1],    [2,1,0,-1],     [-2,-1,0,1] ]];
		
		private var _lastRun:int = flash.utils.getTimer();
		private var _downInterval:int = 300;
		private var _board:Array = new Array(Config.BOARD_WIDTH);
		private var _currentBox:Array = new Array(4);
		private var _currentType:int = 0;
		private var _currentState:int = 0;
		private var _downPos:Point;
		
		public function Game()
		{
			super();
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);		
		}
		
		private function onAddedToStage(evt:Event):void
		{
			removeEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);

			var x:int;
			for (x = 0; x < Config.BOARD_HEIGHT; ++x) {
				_board[x] = new Array(Config.BOARD_HEIGHT);
			}
			
			for (x = 0; x < Config.BOARD_WIDTH; ++x) {
				for (var y:int = 0; y < Config.BOARD_HEIGHT; ++y) {
					_board[x][y] = new Box(x, y);
				}
			}
			
			for (var i:int = 0; i < 4; ++i) {
				_currentBox[i] = new Box();
			}
			
			_uiMainMenu = new MainMenu(this);
			addChild(_uiMainMenu);
			_uiMainMenu.Enter();

			_uiGameScene = new GameScene(this);
		}
		
		public function EnterGame():void
		{
			_uiMainMenu.Exit();
			removeChild(_uiMainMenu);
			
			addChild(_uiGameScene);
			_uiGameScene.Enter();
			
			addEventListener(starling.events.TouchEvent.TOUCH, onTouch);
		}
		
		public function ExitToTitle():void
		{
			if (hasEventListener(starling.events.TouchEvent.TOUCH))
				removeEventListener(starling.events.TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(evt:TouchEvent):void
		{
			var t:Touch = evt.getTouch(this, TouchPhase.BEGAN);
			if (t) {
				var pos:Point = t.getLocation(this);
				_downPos = pos;
			}
			
			t = evt.getTouch(this, TouchPhase.ENDED);
			if (t && _downPos != null) {
				pos = t.getLocation(this);
				
				if (Math.abs(pos.x - _downPos.x) < 100 && Math.abs(pos.y - _downPos.y) < 100) {
					var middle:int = Config.SCREEN_WIDTH * 0.5;
					if (pos.x < middle) {
						moveLeft();
						return;
					}
					else if (pos.x > middle) {
						moveRight();
						return;
					}
				}
				else {
					if (pos.y > _downPos.y) {
						dropToButtom();
						return;
					}
					else if (pos.y < _downPos.y) {
						rotation();
						return;
					}
				}
			}
		}
		
		private function rotation():void
		{
			var newX:Array = new Array(4);
			var newY:Array = new Array(4);
			var minX:int = 0;
			var maxX:int = Config.BOARD_WIDTH - 1;
			var minY:int = 0;
			var maxY:int = Config.BOARD_HEIGHT - 1;
			
			for (var i:int = 0; i < 4; ++i) {
				newX[i] = _currentBox[i].GetX() + ROT_X[_currentType][_currentState][i];
				newY[i] = _currentBox[i].GetY() + ROT_Y[_currentType][_currentState][i];
				
				if (newX[i] < minX)
					minX = newX[i];
				if (newX[i] > maxX)
					maxX = newX[i];
				if (newY[i] < minY)
					minY = newY[i];
				if (newY[i] > maxY)
					maxY = newY[i];
			}
			
			if (minX < 0) {
				for (i = 0; i < 4; ++i) {
					newX[i] -= minX;
				}
			}
			else if (maxX > Config.BOARD_WIDTH - 1) {
				for (i = 0; i < 4; ++i) {
					newX[i] -= (maxX - Config.BOARD_WIDTH + 1);
				}
			}
			
			if (minY < 0) {
				for (i = 0; i < 4; ++i) {
					newY[i] -= minY;
				}
			}
			else if (maxY > Config.BOARD_HEIGHT - 1) {
				for (i = 0; i < 4; ++i) {
					newY[i] -= (maxY - Config.BOARD_HEIGHT + 1);
				}
			}
			
			for (i = 0; i < 4; ++i) {
				if (!_board[newX[i]][newY[i]].IsClear())
					return;
			}
			
			for (i = 0; i < 4; ++i) {
				_currentBox[i].SetPosition(newX[i], newY[i]);
			}
			
			_currentState++;
			if (_currentState > 3)
				_currentState = 0;
		}
		
		private function moveLeft():void
		{
			var move:Boolean = true;
			for (var i:int = 0; i < 4; ++i) {
				var x:int = _currentBox[i].GetX() - 1;
				var y:int = _currentBox[i].GetY();
				
				if (x >= 0 && _board[x][y].IsClear()) {
					continue;
				}
				
				move = false;
				break;
			}
			
			if (move) {
				for (i = 0; i < 4; ++i) {
					x = _currentBox[i].GetX();
					y = _currentBox[i].GetY();
					_currentBox[i].SetPosition(x - 1, y);
				}
			}
		}
		
		private function moveRight():void
		{
			var i:int, x:int, y:int;
			var move:Boolean = true;
			for (i = 0; i < 4; ++i) {
				x = _currentBox[i].GetX() + 1;
				y = _currentBox[i].GetY();
				
				if (x < Config.BOARD_WIDTH && _board[x][y].IsClear()) {
					continue;
				}
				
				move = false;
				break;
			}
			
			if (move) {
				for (i = 0; i < 4; ++i) {
					x = _currentBox[i].GetX();
					y = _currentBox[i].GetY();
					_currentBox[i].SetPosition(x + 1, y);
				}
			}
		}
		
		
		public function NewGame():void
		{
			for (var x:int = 0; x < Config.BOARD_WIDTH; ++x) {
				for (var y:int = 0; y < Config.BOARD_HEIGHT; ++y) {
					_board[x][y].Clear(this);
				}
			}
			
			createBox();
		}
		
		private function createBox():void
		{
			var color:int = Math.floor(Math.random() * 3);
			for (var i:int = 0; i < 4; ++i) {
				_currentBox[i].SetColor(color, this);
			}
			
			_currentState = 0;
			_currentType = Math.floor(Math.random() * 7);
			switch (_currentType) {
				case 0:
					_currentBox[0].SetPosition(6, 0);
					_currentBox[1].SetPosition(7, 0);
					_currentBox[2].SetPosition(8, 0);
					_currentBox[3].SetPosition(7, 1);					
					break;
				
				case 1:
					_currentBox[0].SetPosition(6, 0);
					_currentBox[1].SetPosition(6, 1);
					_currentBox[2].SetPosition(6, 2);
					_currentBox[3].SetPosition(7, 2);
					break;
				
				case 2:
					_currentBox[0].SetPosition(7, 0);
					_currentBox[1].SetPosition(7, 1);
					_currentBox[2].SetPosition(7, 2);
					_currentBox[3].SetPosition(6, 2);
					break;
				
				case 3:
					_currentBox[0].SetPosition(6, 0);
					_currentBox[1].SetPosition(6, 1);
					_currentBox[2].SetPosition(7, 1);
					_currentBox[3].SetPosition(7, 2);
					break;
				
				case 4:
					_currentBox[0].SetPosition(7, 0);
					_currentBox[1].SetPosition(7, 1);
					_currentBox[2].SetPosition(6, 1);
					_currentBox[3].SetPosition(6, 2);
					break;
				
				case 5:
					_currentBox[0].SetPosition(6, 0);
					_currentBox[1].SetPosition(6, 1);
					_currentBox[2].SetPosition(7, 0);
					_currentBox[3].SetPosition(7, 1);
					break;
					
				case 6:
					_currentBox[0].SetPosition(6, 0);
					_currentBox[1].SetPosition(7, 0);
					_currentBox[2].SetPosition(8, 0);
					_currentBox[3].SetPosition(9, 0);
					break;
				
				default:
					return;
			}
		}
		
		public function Run():void
		{
			var now:int = flash.utils.getTimer();
			if (now - _lastRun < _downInterval)
				return;
			
			_lastRun = flash.utils.getTimer();
			moveDown();			
		}
		
		private function moveDown():Boolean
		{
			var i:int, x:int, y:int;
			
			var move:Boolean = true;
			for (i = 0; i < 4; ++i) {
				x = _currentBox[i].GetX();
				y = _currentBox[i].GetY() + 1;
				
				if (y < Config.BOARD_HEIGHT) {
					if (_board[x][y].GetImage() == null) {
						continue;
					}
				}
				
				move = false;
				break;
			}
			
			if (move) {
				for (i = 0; i < 4; ++i) {
					x = _currentBox[i].GetX();
					y = _currentBox[i].GetY();
					_currentBox[i].SetPosition(x, y + 1);
				}
			}
			else {
				for (i = 0; i < 4; ++i) {
					x = _currentBox[i].GetX();
					y = _currentBox[i].GetY();
					_board[x][y].SetBox(_currentBox[i], this);
				}
				
				checkFull();
				createBox();
			}
			
			return move;
		}
		
		private function dropToButtom():void
		{
			for (var i:int = 0; i < Config.BOARD_HEIGHT; ++i) {
				if (!moveDown())
					return;
			}
		}
		
		private function checkFull():void
		{
			var y:int = 0;
			while (y < Config.BOARD_HEIGHT) {
				var full:Boolean = true;
				for (var x:int = 0; x < Config.BOARD_WIDTH; ++x) {
					if (_board[x][y].IsClear()) {
						full = false;
						break;
					}
				}
				
				if (full) {
					DeleteLine(y);
				}

				++y;
			}
		}
		
		private function DeleteLine(y:int):void
		{
			while (y > 0) {						
				for (var x:int = 0; x < Config.BOARD_WIDTH; ++x) {
					var colorType:int = _board[x][y - 1].GetColorType();
					if (colorType != 0xff)
						_board[x][y].SetColor(colorType, this);
					else
						_board[x][y].Clear(this);
				}
				
				y--;
			}
			
			for (x = 0; x < Config.BOARD_WIDTH; ++x) {
				_board[x][0].Clear(this);
			}
		}
	}
}




