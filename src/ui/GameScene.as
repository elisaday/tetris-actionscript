package ui
{
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.RectangleUtil;
	
	public class GameScene extends Sprite
	{
		private var _game:Game;
				
		public function GameScene(game:Game)
		{
			super();
			
			_game = game;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public function onAddedToStage(evt:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			var img:Image = new Image(Assets.getTexture('TEX_TITLE'));
			addChild(img);
		
			var quad:Quad = new Quad(Config.BOARD_WIDTH_SIZE, Config.BOARD_HEIGHT_SIZE, 0x0);
			quad.alpha = 0.5;
			quad.x = Config.BOARD_X;
			quad.y = Config.BOARD_Y;
			addChild(quad);
		}
		
		public function Enter():void
		{
			addEventListener(starling.events.Event.ENTER_FRAME, onEnterFrame);
			_game.NewGame();
		}
		
		public function Exit():void
		{
			if (hasEventListener(starling.events.Event.ENTER_FRAME))
				removeEventListener(starling.events.Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(evt:Event):void
		{
			_game.Run();
		}
	}
}