package ui
{	
	import feathers.controls.Button;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class MainMenu extends Sprite
	{
		private var _game:Game;
		private var _buttonPlay:Button;
		private var _imgPlay:Image;
		
		public function MainMenu(game:Game)
		{
			super();
			
			_game = game;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public function onAddedToStage(evt:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			var img:Image = new Image(Assets.getTexture('TEX_TITLE'));
			addChild(img);
			
			_buttonPlay = new Button();
			_imgPlay = new Image(Assets.getTexture('TEX_BUTTON_PLAY'));
			_buttonPlay.defaultSkin = _imgPlay;
			addChild(_buttonPlay);
			
			_buttonPlay.addEventListener(starling.events.Event.TRIGGERED, onClickPlay);
		}
		
		public function Enter():void
		{
			_buttonPlay.x = (Config.SCREEN_WIDTH - _imgPlay.width) * 0.5;
			_buttonPlay.y = Config.SCREEN_HEIGHT + _imgPlay.height;
			addEventListener(starling.events.Event.ENTER_FRAME, onEnterFrame);			
		}
		
		public function Exit():void
		{
			if (hasEventListener(starling.events.Event.ENTER_FRAME))
				removeEventListener(starling.events.Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(event:Event):void
		{
			var currentDate:Date = new Date();
			_buttonPlay.y = 700 + (Math.cos(currentDate.getTime() * 0.002) * 25);
		}
		
		private function onClickPlay(evt:Event):void
		{
			_game.EnterGame();
		}
	}
} 