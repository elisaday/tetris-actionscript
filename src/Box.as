package
{
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;

	public class Box
	{
		private var _bg:Quad;
		private var _img:Quad;
		private var _x:int = 0;
		private var _y:int = 0;
		private var _colorType:int = 0xff;
		
		public function Box(x:int = 0, y:int = 0)
		{
			SetPosition(x, y);
		}

		public function SetColor(colorType:int, container:Sprite):void
		{			
			if (_bg == null) {
				_bg = new Quad(Config.BLOCK_SIZE, Config.BLOCK_SIZE, 0xffffff);
				container.addChild(_bg);
			}

			if (_img == null) {
				_img = new Quad(Config.BLOCK_SIZE - 2, Config.BLOCK_SIZE - 2);
				container.addChild(_img);
			}
			
			syncImagePos();
			
			switch (colorType) {
				case 0:
					_img.color = 0xff0000;
					break;
				case 1:
					_img.color = 0x00ff00;
					break;
				case 2:
					_img.color = 0x0000ff;
					break;
				default:
					return;
			}
			
			_colorType = colorType;
		}
		
		public function SetPosition(x:int, y:int):void
		{
			_x = x;
			_y = y;
		
			syncImagePos();
		}
		
		private function syncImagePos():void
		{
			if (_bg != null) {
				_bg.x = _x * Config.BLOCK_SIZE + Config.BOARD_X;
				_bg.y = _y * Config.BLOCK_SIZE + Config.BOARD_Y;
			}
			
			if (_img != null) {	
				_img.x = _x * Config.BLOCK_SIZE + Config.BOARD_X + 1;
				_img.y = _y * Config.BLOCK_SIZE + Config.BOARD_Y + 1;
			}
		}
		
		public function GetX():int
		{
			return _x;
		}
		
		public function GetY():int
		{
			return _y;
		}
		
		public function GetColorType():int
		{
			return _colorType;
		}
		
		public function SetBox(b:Box, container:Sprite):void
		{
			var colorType:int = b.GetColorType();
			SetColor(colorType, container);
		
			var x:int = b.GetX();
			var y:int = b.GetY();
			SetPosition(x, y);
		}
		
		public function GetImage():Quad
		{
			return _img;
		}
		
		public function Clear(container:Sprite):void
		{
			if (_img)
				container.removeChild(_img);
			
			if (_bg)
				container.removeChild(_bg);
			
			_img = null;
			_bg = null;
			_colorType = 0xff;
		}
		
		public function IsClear():Boolean
		{
			return _img == null && _bg == null;
		}
	}
}



