package {
	public class Config {
		public static const SCREEN_WIDTH:int = 640;
		public static const SCREEN_HEIGHT:int = 960;
		
		public static const BLOCK_SIZE:int = 30;
		public static const BOARD_WIDTH:int = 15;
		public static const BOARD_HEIGHT:int = 25;
		
		public static const BOARD_WIDTH_SIZE:int = BLOCK_SIZE * BOARD_WIDTH;
		public static const BOARD_HEIGHT_SIZE:int = BLOCK_SIZE * BOARD_HEIGHT;
		
		public static const BOARD_X:int = (SCREEN_WIDTH - BOARD_WIDTH_SIZE) * 0.5;
		public static const BOARD_Y:int = (SCREEN_HEIGHT - BOARD_HEIGHT_SIZE) * 0.5;
	};
};