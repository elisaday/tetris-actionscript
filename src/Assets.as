package
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	
	import starling.textures.Texture;

	public class Assets
	{
		[Embed(source = "../media/texture/title.jpg")]
		public static const TEX_TITLE:Class;
		
		[Embed(source="../media/texture/button_play.png")]
		public static const TEX_BUTTON_PLAY:Class;
		
		private static var _dictTexture:Dictionary = new Dictionary();
				
		public static function getTexture(name:String):Texture
		{
			if (_dictTexture[name] == undefined) {
				var bmp:Bitmap = new Assets[name]();
				var tex:Texture = Texture.fromBitmap(bmp);
				_dictTexture[name] = tex;
				return tex;
			}
			
			return _dictTexture[name];
		}
	}
}

